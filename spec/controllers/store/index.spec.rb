require_relative "../../spec-helpers"

describe Application do
  let(:app) { Application.new }
  
  before :each do
    Seed.create_seed(id: '1', title: 'first', timestamp: 1, views: 4) 
    Seed.create_seed(id: '2', title: 'second', timestamp: 2, views: 3)
    Seed.create_seed(id: '3', title: 'second', timestamp: 3, views: 2)
    Seed.create_seed(id: '4', title: 'third', timestamp: 4, views: 1)

  end
  after(:each) {Post.clear} 
  
  context "gets all data" do
    let(:response) { get "/store" }
    let(:body) {JSON.parse(response.body)}
    
    
    it { expect(response.status).to eq 200 }
    it { expect(body.length).to eq 4 }
    it 'matches first post' do
      post = Post.all[0]
      expect(body[0]['id']).to eq post.id
      expect(body[0]['title']).to eq post.title
      expect(body[0]['content']).to eq post.content
      expect(body[0]['views']).to eq post.views
      expect(body[0]['timestamp']).to eq post.timestamp
    end
  end

  context "gets data with EQUAL query" do
    let(:response) { get "/store?query=EQUAL(title, \"second\")" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['2', '3'] }
  end

  context "gets data with GREATER_THAN query" do
    let(:response) { get "/store?query=GREATER_THAN(views,2)" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['1', '2'] }
  end

  context "gets data with LESS_THAN query" do
    let(:response) { get "/store?query=LESS_THAN(views,2)" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['4'] }
  end

  
  context "gets data with NOT query" do
    let(:response) { get "/store?query=NOT(LESS_THAN(views,2))" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['1', '2', '3'] }
  end

  
  context "gets data with AND query" do
    let(:response) { get "/store?query=AND(LESS_THAN(views,2),NOT(EQUAL(id,\"2\")))" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['4'] }
  end

  context "gets data with OR query" do
    let(:response) { get "/store?query=OR(LESS_THAN(views,2),NOT(EQUAL(id,\"2\")))" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['1', '3', '4'] }
  end

  context "gets data with NESTED query" do
    let(:response) { get "/store?query=AND(AND(LESS_THAN(timestamp,4),NOT(EQUAL(views,3))),OR(EQUAL(id,\"1\"),EQUAL(title,\"second\")))" }
    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 200 }
    it { expect(body.map {|p| p['id']}).to match_array ['1', '3'] }
  end

  context "returns correct error for silly request" do
    let(:response) { get "/store?query=not-even-trying" }
    let(:body) { JSON.parse(response.body) }

    it { expect(response.status).to eq 422 }
    it { expect(body["message"]).to eq 'invalid request' }
  end

  context "returns correct error for invalid comparator" do
    let(:response) { get "/store?query=LYGU(id, \"3\")" }
    let(:body) {JSON.parse(response.body)}

    it { expect(response.status).to eq 422 }
    it { expect(body["message"]).to eq 'no comparator with name "LYGU"' }
  end

  context "returns correct error for invalid param value" do
    let(:response) { get "/store?query=EQUAL(views,\"one thousand and one\")" }
    let(:body) { JSON.parse(response.body) }

    it { expect(response.status).to eq 422 }
    it { expect(body["message"]).to eq "invalid value for Integer(): \"\\\"one thousand and one\\\"\"" }
  end

end 