require_relative "../../spec-helpers"

describe Application do
  let(:app) { Application.new }
  after(:each) {Post.clear} 
  
  context "creates new post with valid params" do
    let!(:response) do
      post "/store", {
        "id": "1",
        "title": "first",
        "content": "content",
        "views": 0,
        "timestamp": 1000
      }.to_json, { 'CONTENT_TYPE' => 'application/json' }
    end

    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 201 }
    it 'matches first post' do
      expect(body).to include Post.all[0].to_h.map { |k, v| [k.to_s, v] }.to_h
    end
  end

  context "Modifies existing post with same id" do
    let!(:response1) do
      post "/store", {
        "id": "1",
        "title": "first",
        "content": "content",
        "views": 0,
        "timestamp": 1000
      }.to_json, { 'CONTENT_TYPE' => 'application/json' }
    end
    let!(:response2) do
      post "/store", {
        "id": "1",
        "title": "second",
        "content": "content",
        "views": 0,
        "timestamp": 1000
      }.to_json, { 'CONTENT_TYPE' => 'application/json' }
    end



    let(:body1) {JSON.parse(response1.body)}
    let(:body2) {JSON.parse(response2.body)}
    
    it { expect(response1.status).to eq 201 }
    it { expect(response2.status).to eq 201 }
    
    it {expect(body1['id']).to eq '1'}
    it {expect(body2['id']).to eq '1'}
    it {expect(body1['title']).to eq 'first'}
    it {expect(body2['title']).to eq 'second'}
    it {expect(Post.all.count).to eq 1}

  end



  context "returns correct error with missing params" do
    let!(:response) do
      post "/store", {
        "id": "1",
        "title": "first",
        "timestamp": "1000"
      }.to_json, { 'CONTENT_TYPE' => 'application/json' }
    end

    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 422 } 
    it { expect(body['views'][0]).to eq 'is missing' } 
    it { expect(body['content'][0]).to eq 'is missing' } 
  end

  
  context "returns correct error with invalid params" do
    let!(:response) do
      post "/store", {
        "id": "1",
        "title": 10,
        "content": "content",
        "views": 0,
        "timestamp": "one thousand"
      }.to_json, { 'CONTENT_TYPE' => 'application/json' }
    end

    let(:body) {JSON.parse(response.body)}
    
    it { expect(response.status).to eq 422 } 
    it { expect(body['title'][0]).to eq 'must be a string' } 
    it { expect(body['timestamp'][0]).to eq 'must be an integer' } 
  end

end 