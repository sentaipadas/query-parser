require "rack/test"
require_relative '../application'

require 'rspec/expectations'

RSpec::Matchers.define :be_a_multiple_of do |expected|
  match do |actual|
    actual % expected == 0
  end
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
end