require 'faker'
require_relative './models/data'

module Seed 
  Faker::Config.random = Random.new(42)

  def self.create_seed(id: nil ,title: nil ,content: nil ,views: nil ,timestamp: nil )
    Post.new({
      id: id || Faker::Alphanumeric.alphanumeric(number: 10),
      title: title || Faker::Movies::HitchhikersGuideToTheGalaxy.specie,
      content: content || Faker::Movies::HitchhikersGuideToTheGalaxy.quote,
      views: views || Faker::Number.number(digits: 4),
      timestamp: timestamp || Faker::Time.between(from: DateTime.now - 60, to: DateTime.now).to_i
    })
  end
end

