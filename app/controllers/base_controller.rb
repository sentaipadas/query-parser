require 'json'

class BaseController
  attr_reader :request

  def initialize(request)
    @request = request
  end

  private

  def build_response(body, status: 200)
    [status, { "Content-Type" => "application/json" }, [body.to_h.to_json]]
  end

  def params
    request.params
  end
end