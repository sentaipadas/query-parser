require_relative './base_controller'

require_relative '../models/query'
require_relative '../models/data'

require_relative '../models/validation/validation_error'

class StoreController < BaseController
  def index
    begin
      data = if params['query'] 
        query = HeadQuery.new params['query']
        Post.all.filter {|p| query.data_match? p} 
      else
        Post.all
      end
      build_response data
    
    rescue ValidationError => e
      build_response e.data, status: e.status
    end
  end

  def create
    post = Post.new params[:data]
    if post.errors.empty?  
      build_response post, status: 201
    else 
      build_response post.errors, status: 422
    end
  end
end