require_relative './validation/post_contract'

class DataCollection 
  def initialize(data = [])
    @objects = data
  end

  def <<(data)
    @objects << data 
  end

  def [](key)
    @objects[key]
  end

  def to_h
    @objects.map {|o| o.to_h}
  end

  def filter &block
    DataCollection.new @objects.filter &block
  end

  def find(id)
    @objects.detect{|o| o.id == id}
  end
  def count 
    @objects.count
  end
end

class Post < Struct.new(:id, :title, :content, :views, :timestamp, keyword_init: true)

  @@all = DataCollection.new

  def initialize(*args)
    @params = PostSchema.call *args
    if @params.errors.empty?
      existing = @@all.find(@params.to_h[:id])
      super(@params.to_h)
      if existing
        @params.to_h.each {|k,v| existing.send "#{k}=", v}
      else
        @@all << self
      end
    end
  end

  def errors
    @params.errors
  end

  def self.all 
    @@all
  end

  def self.clear
    @@all = DataCollection.new
  end
end