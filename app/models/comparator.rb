require_relative './validation/validation_error'

class Comparator < Struct.new(:name, :operator, :nested, :direct, :single, keyword_init: true)
  @@objects = []

  def initialize(*args)
    super(*args)
    @@objects << self
  end

  def direct?
    direct
  end

  def single?
    single
  end

  def self.find(name)
    object = @@objects.detect {|o| o.name == name}
    raise ValidationError.new message: "no comparator with name \"#{name}\"" unless object
    object
  end
end

Comparator.new name: 'EQUAL', direct: true, operator: lambda {|*args| args[0]==args[1]}
Comparator.new name: 'LESS_THAN', direct: true, operator: lambda {|*args| args[0]<args[1]}
Comparator.new name: 'GREATER_THAN', direct: true, operator: lambda {|*args| args[0]>args[1]}

Comparator.new name: 'AND', direct: false, operator: lambda {|*args| args[0]&&args[1]}
Comparator.new name: 'OR', direct: false, operator: lambda {|*args| args[0]||args[1]}
Comparator.new name: 'NOT', direct: false, single: true, operator: lambda {|*args| !args[0]}
