require 'dry-types'

require_relative './validation_error'

INTEGER_PARAMS = [
  'views', 
  'timestamp'
]

STRING_PARAMS = [
  'id', 
  'title',
  'content'
]

PARAMS = INTEGER_PARAMS + STRING_PARAMS

module Types 
  include Dry.Types()
  Properties = Types::String.enum(*PARAMS)

  StringValue = Types::String.constructor do |str|
    str ? str.strip.chomp.delete_prefix('"').delete_suffix('"') : strw
  end  
end

class QueryParams < Struct.new(:property, :value, keyword_init: true)
  
  def initialize(property:, value:)
    begin 
      _property = Types::Properties[property]
      if INTEGER_PARAMS.include? _property
        _value = Types::Coercible::Integer[value]
      elsif STRING_PARAMS.include? _property
        _value = Types::StringValue[value]
      end
    rescue Dry::Types::ConstraintError, Dry::Types::CoercionError => e
      raise ValidationError.new message: e.message
    end
    super property: _property, value: _value 
  end
end

