require 'dry-schema'

PostSchema = Dry::Schema.Params do
  required(:id).maybe(:string)
  required(:title).maybe(:string)
  required(:content).maybe(:string)
  required(:views).filled(:integer)
  required(:timestamp).filled(:integer)
end