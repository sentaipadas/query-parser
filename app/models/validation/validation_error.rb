class ValidationError < StandardError
  attr_reader :data
  attr_reader :status
  
  def initialize(data, status: 422)
    @data = data
    @status = status
  end
  
end