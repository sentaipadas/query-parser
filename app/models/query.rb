require_relative './comparator'
require_relative './validation/query_params'

require_relative './validation/validation_error'

class Query
  attr_accessor :name, :params, :children, :errors

  def initialize(name)
    self.name = name
    self.children = []
  end

  def data_match? data_entry
    if comparator.direct?
      comparator.operator.call(data_entry[params[:property]], params[:value])
    else 
      validation_values = children.map {|c| c.data_match?(data_entry)}  
      comparator.operator.call(*validation_values)
    end
  end

  def comparator 
    @comparator ||= Comparator.find name
  end

end

class HeadQuery < Query

  def initialize(query_string) 
    super('')
    self.errors = []
    begin
      self.parse_query query_string
    rescue StandardError => e
      raise e if e.instance_of? ValidationError 
      raise ValidationError.new message: 'invalid request'
    end
  end

  def parse_query query_string
    return nil unless query_string

    query_stack = []
    self.name, query_string = query_string.split('(', 2)
    query_stack.push self
    while query_stack.count > 0
      query = query_stack.pop
      query_string = query_string.gsub(/^(\)|,)*/, '')
      if query.comparator.direct?
        property, value, query_string = query_string.split(/(?="(?=.*?)")|(?=\))|,/, 3)
        query.params = QueryParams.new property: property, value: value 
      else
        child_name, query_string = query_string.split('(', 2)
        child_query = Query.new child_name
        query.children << child_query
        query_stack << query if !query.comparator.single? && query.children.count < 2
        query_stack << child_query
      end

    end
    self
  end
end