class Router
  def initialize(request)
    @request = request
  end

  def route_info
    @route_info ||= begin
      resource = path_fragments[0] || "base"
      action = find_action
      { resource: resource, action: action }
    end
  end

  def find_action
      @request.get? ? :index : :create
  end

  def path_fragments
    @fragments ||= @request.path.split("/").reject { |s| s.empty? }
  end

  def controller_name
    "#{route_info[:resource].capitalize}Controller"
  end

  def controller_class
    Object.const_get(controller_name)
  rescue NameError
    nil
  end

  def route!
    if klass = controller_class
      add_route_info_to_request_params!
      add_body_to_params!

      controller = klass.new(@request)
      action = route_info[:action]

      if controller.respond_to?(action)
        return controller.public_send(action)
      end
    end

    not_found
  end

  private

  def add_route_info_to_request_params!
    @request.params.merge!(route_info)
  end

  def add_body_to_params!
    if @request.post?
      body = JSON.parse @request.body.read
      @request.params.merge!(data: body)
    end
  end

  def not_found(msg = "Not Found")
    [404, { "Content-Type" => "text/plain" }, [msg]]
  end

end