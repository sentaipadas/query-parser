## Setup 
This project was built using `ruby 2.6.4` and `rake`, version is specified in the `Gemfile`, so if another ruby version is desired it should be replaced there.

1. To download necessary dependencies run:
```
bundle
```

## Launch 

To start the live server run:
```
rakeup config.ru
```

## Testing 

To execute tests run:

```
rspec --pattern ./spec/**/*.spec.rb
```

## Design

In this project the most minimal ruby framework is used. It is a simple query parser and persistence, more complex routing and most other fancy features from Rails are not required. Rack provides the bare minimum for creating an API in addition with a few helpful methods, and with a little boilerplate I got my app running.  

App is centered around 2 models: Post and Query.
1. Query is responsible for parsing the get request `query` param and constructing a tree of comparators with children as nested queries inside AND, OR or NOT queries. As we know that each nested comparator has 1 or 2 children, result is a binary tree and query can be parsed by using DFS by following a few simple rules of tracking commas and opening brackets. The algorithm is implemented in `HeadQuery#parse_query`.   
After parsing filtering is done simply by traversing the tree using recursion and aggregating the values from the bottom following comparator rules until a single booloean value is returned for each data entry indicating whether it satisfies the query or not. Performance of this can be optimized by using SQL or other querying language, i.e. we could compose a database query in one traversal and execute it to filter the data.
2. Post a simple model for storing data, it has a `@@all` class variable storing all instances of the class as a `DataCollection` object which can be more easily filtered itself and serialized.  
  
Robust input validation is done using the `dry-validation` gem, which provides easy schema interface as well as descriptive error messages.

Testing is behaviourly driven and only tests outside interactions with the API for both valid and invalid requests. 